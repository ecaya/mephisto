#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys,random
from Sidemethods import Misc

from direct.showbase.ShowBase import ShowBase
from direct.interval.IntervalGlobal import *
from direct.task import Task
from direct.actor.Actor import Actor
from panda3d.core import WindowProperties,AmbientLight, DirectionalLight,Point3,Vec3, VBase4, BitMask32, Mat4
from panda3d.core import CollisionNode,CollisionSphere,CollisionBox,\
						CollisionHandlerPusher,CollisionTraverser,CollisionHandlerEvent,CollisionHandlerQueue,\
						CollisionRay,CollisionPlane,Plane,CollisionSegment,CollisionPolygon

class View(ShowBase):
	def __init__(self,master):
		ShowBase.__init__(self)
		self.disableMouse()
		self.master = master
		#Actions à envoyer au model
		self.mobs = dict()
		self.visualEvents = dict()
		self.visualEvents["hero"] = dict()
		#Utilisé pour les objets visuels (Hero & Mobs)
		View.render = self.render
		View.loader = self.loader
		View.camera = self.camera
		#Bitmask qui détermine quels solides de collision intéragit entre eux
		worldMask = BitMask32(0x1)
		attackMask = BitMask32(0x2)
		self.maskList = { "world" : worldMask, "attack" : attackMask }
		View.maskList = self.maskList
		#CollisionTraverser, et ses handlers analyse les collisions chaque frame
		self.cTrav = CollisionTraverser()
		self.handler = CollisionHandlerEvent()
		self.queue = CollisionHandlerQueue()
		
		View.cTrav = self.cTrav
		View.handler = self.handler
		View.queue = self.queue
		
		View.accept = self.accept
	
	def updateView(self,task):
		#on envoi les evenements des collisions et des touches du joueur au model
		visualActions = self.master.call("model","updateGame",self.visualEvents)
		#on traite les decisions qui sont récupérés
		#Pour le héro
		for name,mob in self.mobs.items():
			if name in visualActions:
				mob.updateVisual(visualActions[name])
				self.visualEvents[name] = mob.refreshEvents()
				if(not mob.alive):
					del self.mobs[name]
		self.visualEvents["hero"] = self.hero.refreshEvents()
		#On traverse les collision pour gérer les collisions avec l'environement (camera et entitées)
		#On rajoute l'ajustement de rotation de la camera (après le test de collision) pour l'envoyer au model
		
		if(self.hero != None):
			self.hero.updateVisual(visualActions["hero"])
			#Et les ennemis
			if(not self.hero.alive):
				del self.hero
				sys.exit()
			#La camera aussi
			self.spinCameraTask()
			self.hero.updateEvents("rotation",self.camera.getParent().getParent().getH())
		
		self.cTrav.traverse(self.render)
		#On utilise le Queue handler plutot que l'event pour 
		#parcourir les collisions qu'on lui a assigné
		#Si il y a plus qu'une collision, on additione leur normal et le "shove" (la poussée contraire a appliquer)
		#Pour donner un semblant d'effet de collision "réelle"
		self.updateCollisions()
		
		return Task.cont
	
	def updateCollisions(self):
		normal = Vec3(0.0,0.0,0.0)
		length = 0
		#Test de collision pour le Hero contre l'environement
		for i in range(self.queue.getNumEntries()):
			entry = self.queue.getEntry(i)
			name = entry.getFromNodePath().getName()
			normal += entry.getSurfaceNormal(entry.getIntoNodePath())
			surface_p = entry.getSurfacePoint(self.render)
			surface_n = normal
			interior_p = entry.getInteriorPoint(self.render)
			length += (surface_p - interior_p).length()
			shove = surface_n * length
			newPos = self.hero().getPos()
			mat = self.hero().getMat()
			newPos += mat.xformVec(shove)
			posM = {"x":newPos.x,"y":newPos.y}
			self.hero.updateEvents("blocked",posM)
				
		
	def createLevel(self,starting_params):
		#Ajout des entites d'environement, la lumiere, etc.
		self.addSkybox()
		self.addLight("AmbientLight",(0,0,35))
		self.addEnvironement()
		
		#Ajout des entites
		self.hero = VisualHero()
		self.addMobs(starting_params["mobs"])
		self.setCamera()

		self.addColliders()
		#Pour afficher les collisions
		#self.cTrav.showCollisions(View.render)
		
		self.addEvents()
		
	def addSkybox(self):
		self.sky = self.loader.loadModel("skybox/skybox.egg")
		self.sky.setTwoSided(1)#Donne les textures de chaque côté du skybox
		self.sky.setScale(100)
		self.sky.setBin("background",0)
		#Le DepthWrite permet de lui donner L'impression qu'il est infini (tant qu'on reste a l'intérieur evidemment)
		self.sky.setDepthWrite(0)
		self.sky.reparentTo(self.render)
		self.sky.setCompass()
		
	def addLight(self,mode,pos):
		if (mode == "AmbientLight"):
			alight = AmbientLight('my dlight')
			alight.setColor(VBase4(0.2,0.2,0.2,1))
			alnp = self.render.attachNewNode(alight)
			alnp.setPos(pos)
			self.render.setLight(alnp)

		dlight = DirectionalLight('dlight')
		dlight.setColor(VBase4(1, 1, 1, 1))
		dlnp = render.attachNewNode(dlight)
		dlnp.setHpr(0, -60, 0)
		self.render.setLight(dlnp)
		
	def addEnvironement(self):
		#environement
		self.plane = self.loader.loadModel("models/testgrass1")
		self.plane.setScale(50,50,1)
		self.plane.reparentTo(self.render)
		self.plane.setPos(0,0,25)
		self.plane.setTwoSided(1)
		node = CollisionNode('BlockCollisionNode'+ str(random.random()))
		node.addSolid(CollisionBox(Point3(0,0,-1),0.5,0.5,1))
		node.setFromCollideMask(0)
		node.setIntoCollideMask(self.maskList["world"])
		pnodePath = self.plane.attachNewNode(node)
		
		self.addPlatform((5,5,1),(3,2,25),"models/textureblock")
		self.addPlatform((10,10,1),(-14,10,28),"models/textureblock")
		self.addPlatform((3,3,1),(15,0,26),"models/textureblock")
		
		self.addOuterWalls()
		
	def addPlatform(self,scale,pos,model):
		block = self.loader.loadModel(model)
		block.reparentTo(self.render)
		block.setScale(scale)
		block.setPos(pos)
		
		node = CollisionNode('BlockCollisionNode'+ str(random.random()))
		node.addSolid(CollisionBox(Point3(0,0,0.5),0.5,0.5,0.5))
		node.setFromCollideMask(0)
		node.setIntoCollideMask(self.maskList["world"])
		bnodePath = block.attachNewNode(node)
		
	def addOuterWalls(self):
		self.addPlatform((55,50,25),(0,50,25),"models/textureblock")
		self.addPlatform((50,55,25),(50,0,25),"models/textureblock")
		self.addPlatform((55,50,25),(0,-50,25),"models/textureblock")
		self.addPlatform((50,55,25),(-50,0,25),"models/textureblock")

	def addMobs(self,mobpositions):
		for i in range(0,5):
			list = (mobpositions["enemy"+str(i)]["x"],mobpositions["enemy"+str(i)]["y"],mobpositions["enemy"+str(i)]["z"])
			self.mobs["enemy"+str(i)] = VisualMonster("enemy"+str(i),list)
	
	def setCamera(self):
		heading_node = self.hero().attachNewNode("HeadingNode")
		heading_node.setPos(0,0,1.5)
		pitch_node = heading_node.attachNewNode("PitchNode")
		self.camera.reparentTo(pitch_node)
		self.camera.setPos(0,8,8)
		self.camera.lookAt(self.camera.getParent())
		
		self.taskMgr.add(self.updateView, "SpinCameraTask")

	def addColliders(self):
		#Ajout des patterns pour le HandlerEvent
		#Les patterns permettent ensuite d'ajouter au gestionnaire d'évènements
		#(la méthode self.accept()) d'accepter et de lancer des évènements pour des
		#possibilitées précises : par exemple je pourrais dire, toutes les collisions
		#que le corps fait avec n'importe quel autre, envoie le à cette méthode avec ces paramètres
		#Ou encore préciser que je veut absolument gérer une collision si c'est un type x qui rentre dans un type y
		#Pour voir des exemples : regarder addEvents
		self.handler.addInPattern('%fn-into')
		self.handler.addInPattern('into-%in')
		self.handler.addAgainPattern('%fn-again')
		self.handler.addOutPattern('%fn-out')
		
		#Camera
		node = CollisionNode("CollisionCameraNode")
		node.addSolid(CollisionSegment(Point3(0,0,0),Point3(0,8,8)))
		node.setIntoCollideMask(0)
		node.setFromCollideMask(self.maskList["world"])
		CameraCollidePath = self.camera.getParent().attachNewNode(node)
		
		self.cTrav.addCollider(CameraCollidePath,self.handler)
		self.hero.addColliders()
		for key in self.mobs:
			self.mobs[key].addColliders()
		
	def addEvents(self):
		self.hero.addEvents()
		for key in self.mobs:
			self.mobs[key].addEvents()
		self.accept("escape", sys.exit)
		#Camera Collision
		self.accept("CollisionCameraNode-into",self.cameraCollision,["add"])
		self.accept("CollisionCameraNode-again",self.cameraCollision,["add"])
		self.accept("CollisionCameraNode-out",self.cameraCollision,["del"])
		self.camera.getParent().getParent().setH(self.hero.model,0)
		
	#Methodes de la Camera
	#Permet de faire tourner la camera autour du hero
	def spinCameraTask(self):
		#win = Window, get pointer retourne la souris
		#props = proprietes de l'ecran
		#on utilise le centre pour remettre constamment la souris au milieu
		props = WindowProperties()
		props.setCursorHidden(True)
		self.win.requestProperties(props)
		props = self.win.getProperties()
		centerx = props.getXSize() / 2
		centery = props.getYSize() / 2
		md = self.win.getPointer(0)
		x = md.getX()
		y = md.getY()
		
		#Facteur de sensibilite
		factor = 0.2
		
		#Si on arrive a bouger la souris au centre
		if self.win.movePointer( 0, centerx, centery ):
				#On calcule la difference de distance et on l'additione a la rotation du perso
				deltaHeading = ( x - centerx ) * factor
				#model car il tourne sur lui meme vers la gauche ou la droite
				self.camera.getParent().getParent().setH(self.camera.getParent().getParent(),-deltaHeading)
				deltaPitch = ( y - centery ) * factor
				#le parent de camera, car ainsi la camera tourne autour du Panda plutot que sur lui meme
				# sans pour autant changer la rotation du model (En plus d'appliquer des limites
				if(self.camera.getParent().getP()+deltaPitch > 0):
					self.camera.getParent().setP(0)
				elif(self.camera.getParent().getP()+deltaPitch < -80):
					self.camera.getParent().setP(-80)
				else:
					self.camera.getParent().setP(self.camera.getParent(),deltaPitch)

	#Permet de replacer la camera si celle-ci rentre en collision avec l'environement
	def cameraCollision(self,action,entry):
		if(action == "add"):
			globalPos = entry.getSurfacePoint(self.camera.getParent())
			self.camera.setPos(globalPos)
		else:
			self.camera.setPos(0,8,8)

class VisualEntity():
	def __init__(self,name,pos=(0,0,25)):
		self.name = name
		self.alive = True
		self.start_pos = pos
		self.events = dict()
		self.createVisual()
		
	def __call__(self):
		return self.node
		
	def die(self):
		self.alive = False
		self.node.removeNode()
		
	def createVisual(self):
		self.node = View.render.attachNewNode("ModelNode")
		self.node.reparentTo(View.render)
		self.node.setPos(self.start_pos)
		self.node.setTwoSided(1)
		
	def refreshEvents(self):
		self.events = dict()
		return self.events

	def updateEvents(self,what,event):
		if(what == "limitZ"):
			self.events.update(limitZ=event)
		elif(what == "fall"):
			self.events.update(fall=event)
		elif(what == "blocked"):
			self.events.update(blocked=event)
		elif(what == "hurt"):
			self.events.update(hurt=event)


	def updateVisual(self,actions):
		if("move" in actions):
			self.node.setX(actions["move"]["x"])
			self.node.setY(actions["move"]["y"])
		if("jump" in actions):
			self.node.setZ(actions["jump"])
		if("rotate" in actions):
			self.model.setH(actions["rotate"]["h"])
		if("pushback" in actions):
			pos = Point3(actions["pushback"]["x"],actions["pushback"]["y"],actions["pushback"]["z"])
			interval = LerpPosInterval(self.node,0.10,pos)
			interval.start()

class VisualMonster(VisualEntity):
	def __init__(self,name,pos):
		VisualEntity.__init__(self,name,pos)
		
	def updateVisual(self,actions):
		VisualEntity.updateVisual(self,actions)
		if("hurt" in actions):
			if(actions["hurt"]):
				self.model.setColorScale(20, 0, 0, 1)
				self.model.pose("attack",0)
			else:
				self.model.clearColorScale()
		if("move" in actions):
			if self.model.getCurrentAnim() == None:
				self.model.loop('walk')
		else:
			self.model.stop()
			self.model.pose("walk",0)
		if("attack" in actions):
			self.model.play('attack')
		if("active" in actions):
			self.model.find(self.name+"AttackCollisionNode").node().setFromCollideMask(View.maskList["attack"])
		if("inactive" in actions):
			self.model.find(self.name+"AttackCollisionNode").node().setFromCollideMask(0)
		if("die" in actions):
			self.die()
		
	def createVisual(self):
		VisualEntity.createVisual(self)
		self.node.setPos(self.start_pos)
		
		self.model = Actor("models/ratanim",
							{"walk" : "models/ratanim-walk",
							 "attack" : "models/ratanim-attack"})
		self.model.setScale(0.6,0.6,0.6)
		self.model.reparentTo(self.node)
		self.model.pose("walk",0)
	
	def addEvents(self):
		#On ajoute les collisions pour lesquelles ont veut envoyer des évènements
		View.accept(self.name+'CollisionNode-into',self.updateCollisions,["body","add"])
		View.accept(self.name+'CollisionNode-again',self.updateCollisions,["body","add"])
		View.accept(self.name+'DetectionNode-into',self.updateCollisions,["detect","add"])
		View.accept(self.name+'DetectionNode-again',self.updateCollisions,["detect","add"])
		View.accept(self.name+'FeetCollisionNode-out',self.updateCollisions,["feet","del"])
		View.accept('into-'+self.name+'HurtBox',self.updateCollisions,["hurt","add"])
	
	def updateEvents(self,what,event):
		VisualEntity.updateEvents(self,what,event)
		if(what == "neartarget"):
			self.events["neartarget"] = event
	
	def addColliders(self):
		node = CollisionNode(self.name+'CollisionNode')
		node.addSolid(CollisionSphere(0, 0, 1, 0.8))
		node.setIntoCollideMask(0)
		node.setFromCollideMask(View.maskList["world"])
		MonsterCollidePath = self.node.attachNewNode(node)
		#MonsterCollidePath.show()
		View.cTrav.addCollider(MonsterCollidePath,View.handler)
		
		#Hurtbox
		node = CollisionNode(self.name+'HurtBox')
		node.addSolid(CollisionSphere(0,0,1,0.8))
		node.setIntoCollideMask(View.maskList["attack"])
		node.setFromCollideMask(0)
		HurtBoxCollidePath = self.node.attachNewNode(node)
		#HurtBoxCollidePath.show()
		
		node = CollisionNode(self.name+'DetectionNode')
		node.addSolid(CollisionSphere(0, 0, 1, 6))
		node.setIntoCollideMask(0)
		node.setFromCollideMask(BitMask32(0x4))
		MonsterDetectionPath = self.node.attachNewNode(node)
		#MonsterDetectionPath.show()
		View.cTrav.addCollider(MonsterDetectionPath,View.handler)
		
		node = CollisionNode(self.name+'FeetCollisionNode')
		node.addSolid(CollisionSegment(Point3(0,0,0.5),Point3(0,0,-0.5)))
		node.setIntoCollideMask(0)
		node.setFromCollideMask(View.maskList["world"])
		FeetCollidePath = self.node.attachNewNode(node)
		#FeetCollidePath.show()
		View.cTrav.addCollider(FeetCollidePath,View.handler)
		
		node = CollisionNode(self.name+'AttackCollisionNode')
		node.addSolid(CollisionSphere(0,0.3,0,1))
		node.setIntoCollideMask(0)
		node.setFromCollideMask(0)
		AttackCollidePath = self.model.attachNewNode(node)
		#AttackCollidePath.show()
		View.cTrav.addCollider(AttackCollidePath,View.handler)
		#ExposeJoint permet de deplacer le AttackCollidePath selon les transformations du joint (ou bone),
		#et ainsi suivre le mouvement des animations
		self.model.exposeJoint(AttackCollidePath,'modelRoot','Bone.003')
	
	def updateCollisions(self,type,action,entry):
		if(type=="body" or type=="feet"):
			angle = 90
			self.updateEvents("blocked",angle)
		elif(type=="detect"):
			if(action=="add"):
				targetPos = entry.getIntoNodePath().getParent().getPos()
				self.updateEvents("neartarget",targetPos)
		elif(type == "hurt"):
			if(entry.getFromNodePath().getName().find("enemy") == -1):
				position = -(entry.getInteriorPoint(self.node)*3)
				self.updateEvents("hurt",position)
		
class VisualHero(VisualEntity):
	def __init__(self):
		VisualEntity.__init__(self,"hero")
		self.directions = list()

	def createVisual(self):
		VisualEntity.createVisual(self)
		self.model = Actor("models/frogfinal",
							{"walk" : "models/frogfinal-walk",
							 "attack" : "models/frogfinal-attack"})
		self.model.setScale(0.5,0.5,0.5)
		self.model.reparentTo(self.node)
		#Par défaut des noms aux joints/bones sont donnés dans Blender lorsqu'on les crées, Donc ici les Bone.XXX correspondent
		#À ceux des jambes
		self.model.makeSubpart("legs", ["Bone.006","Bone.007","Bone.008","Bone.009"])
		#On peut aussi les renommer soit même (dans Blender) pour se faciliter la tâche
		self.model.makeSubpart("arms", ["upperarm.R","lowerarm.R","wrist.R","upperarm.L","lowerarm.L","wrist.L"])
		self.model.pose("walk",0,partName="legs")
		self.model.pose("attack",0,partName="arms")
		self.control = self.model.getAnimControl("attack","arms")

		
	def updateEvents(self,what,event):
		VisualEntity.updateEvents(self,what,event)
		if(what == "movement"):
			if("movement" not in self.events):
				self.events["movement"] = list((event,))
			else:
				self.events["movement"].append(event)
		elif(what == "rotation"):
			self.events["rotation"] = event
		elif(what == "jump"):
			self.events.update(jump=event)
		elif(what == "attack"):
			self.events.update(attack=event)
			
	def setDirections(self,event):
		if(event[0] == "+"):
			self.directions.append(event[1])
		else:
			self.directions.remove(event[1])
	
		return self.directions
	
	def updateVisual(self,actions):
		VisualEntity.updateVisual(self,actions)
		if("hurt" in actions):
			if(actions["hurt"]):
				self.model.setColorScale(20, 0, 0, 1)
				self.model.pose("attack",0,partName="arms")
			else:
				self.model.clearColorScale()
		if("move" in actions):
			if self.model.getCurrentAnim() == None:
				self.model.loop('walk', partName="legs")
		else:
			self.model.stop()
			self.model.pose("walk",0,partName="legs")
		if("attack" in actions):
			if(actions["attack"]):
				self.control.play()
		if("active" in actions):
			self.model.find("FistCollisionNode").node().setFromCollideMask(View.maskList["attack"])
		if("inactive" in actions):
			self.model.find("FistCollisionNode").node().setFromCollideMask(0)
		if("die" in actions):
			self.die()
	
	def addColliders(self):
		#Collisions reliés à l'environnement
		#Corps du hero
		node = CollisionNode('HeroCollisionNode')
		node.addSolid(CollisionSphere(0, 0, 1, 0.8))
		node.setIntoCollideMask(BitMask32(0x4))
		node.setFromCollideMask(View.maskList["world"])
		HeroCollidePath = self.node.attachNewNode(node)
		#HeroCollidePath.show()
		
		#Hurtbox
		node = CollisionNode('HeroHurtBox')
		node.addSolid(CollisionSphere(0,0,1,0.8))
		node.setIntoCollideMask(View.maskList["attack"])
		node.setFromCollideMask(0)
		HurtBoxCollidePath = self.node.attachNewNode(node)
		#HurtBoxCollidePath.show()
		
		#Head
		node = CollisionNode('HeadCollisionNode')
		node.addSolid(CollisionSegment(Point3(0,0,1),Point3(0,0,2)))
		node.setIntoCollideMask(0)
		node.setFromCollideMask(View.maskList["world"])
		HeadCollidePath = self.node.attachNewNode(node)
		#HeadCollidePath.show()
		
		#Pieds
		node = CollisionNode('FeetCollisionNode')
		node.addSolid(CollisionSegment(Point3(0,0,0.5),Point3(0,0,-0.5)))
		node.setIntoCollideMask(0)
		node.setFromCollideMask(View.maskList["world"])
		FeetCollidePath = self.node.attachNewNode(node)
		#FeetCollidePath.show()
		
		#Poings (pour attaquer)
		node = CollisionNode('FistCollisionNode')
		node.addSolid(CollisionSphere(0,0.8,0,1))
		node.setIntoCollideMask(0)
		node.setFromCollideMask(0)
		FistCollidePath = self.model.attachNewNode(node)
		#FistCollidePath.show()
		#ExposeJoint permet de deplacer le FistCollidePath selon les transformations du joint (ou bone),
		#et ainsi suivre le mouvement des animations
		self.model.exposeJoint(FistCollidePath,'arms','wrist.R')
		
		View.cTrav.addCollider(FeetCollidePath,View.handler)
		View.cTrav.addCollider(HeadCollidePath,View.handler)
		View.cTrav.addCollider(FistCollidePath,View.handler)
		View.cTrav.addCollider(HurtBoxCollidePath,View.handler)
		View.cTrav.addCollider(HeroCollidePath,View.queue)
		
	def addEvents(self):
		#Collision Events
		#On utilise le event manager (avec la methode accept)
		#pour demander au jeu de surveiller les collisions et me laisser gérer ce qui arrive
		#C'est le même principe pour le CollisionHandlerQueue, mais c'est une boucle qu'on appelle
		#une fois par frame pour parcourir les collisions
		View.accept('FeetCollisionNode-into',self.updateCollisions,["feet","add"])
		View.accept('FeetCollisionNode-again',self.updateCollisions,["feet","add"])
		View.accept('FeetCollisionNode-out',self.updateCollisions,["feet","del"])
		View.accept('HeadCollisionNode-into',self.updateCollisions,["head","add"])
		View.accept('into-HeroHurtBox',self.updateCollisions,["hurt","add"])
		
		#Keyboard Events
		#pour mettre-à-jour les messages a envoyer au modele
		View.accept("w", self.updateEvents,["movement","+f"])
		View.accept("s", self.updateEvents,["movement","+b"])
		View.accept("a", self.updateEvents,["movement","+l"])
		View.accept("d", self.updateEvents,["movement","+r"])
		View.accept("w-up", self.updateEvents,["movement","-f"])
		View.accept("s-up", self.updateEvents,["movement","-b"])
		View.accept("a-up", self.updateEvents,["movement","-l"])
		View.accept("d-up", self.updateEvents,["movement","-r"])
		View.accept("space", self.updateEvents,["jump",True])
		
		#Mouse Events
		View.accept("mouse1", self.updateEvents,["attack",True])

	def updateCollisions(self,type,action,entry):
		if(type == "feet"):
			minZ = 0
			#Correspond a la hauteur reel (par rapport a render) de la collision (sur le dessus du model)
			#getTightBounds()[1] correspond au coordonnées maximum du model
			maxz = entry.getIntoNodePath().getParent().getTightBounds()[1].z
			if( action=="add" ):
					minZ = maxz
			self.updateEvents("limitZ",minZ)
		elif(type == "head"):
			if( action=="add" ):
				self.updateEvents("fall",-0.025)
		elif(type == "hurt"):
			if(entry.getFromNodePath().getName() != 'FistCollisionNode'):
				position = -(entry.getInteriorPoint(self.node)*3)
				self.updateEvents("hurt",position)
		
		
	