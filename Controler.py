﻿from View import *
from Model import *


class Controler():

	def __init__(self):
		self.model = Model(self)
		self.view = View(self)
	
	#Methode qui demarre le jeu
	def begin(self):
		self.model.createGame()
		while 1:
			self.view.taskMgr.step()
	
	#Methodes qui appel les fonctions retrouvees dans les instances View et Model
	def call(self,target,method,params = ""):
		method = getattr(getattr(self,target),method)
		if(params != ""):
			result = method(params)
		else:
			result = method()
		return result
		
		
if __name__ == "__main__":
	c = Controler()
	c.begin()