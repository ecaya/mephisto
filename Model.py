#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy,random
from Sidemethods import Misc

class Model():
	def __init__(self,master):
		self.master = master;
		self.visual_actions = dict()
		self.beings = dict()
		
	def createGame(self):
		starting_params = dict()
		self.hero = Hero()
		starting_params["mobs"] = self.createMobs()
		self.master.call("view","createLevel",starting_params)
	
	def updateGame(self,visualInfo):
		self.visual_actions = dict()
		#Autres entités
		for name,being in self.beings.items():
			if (name in visualInfo):
				being.analyzeActions(visualInfo[name])
			self.visual_actions[name] = being.loopActions()
			if(not being.alive):
				del self.beings[name]
		#Hero
		if(self.hero != None):
			self.hero.analyzeActions(visualInfo["hero"])
			self.visual_actions.update(hero=self.hero.loopActions())
			if(not self.hero.alive):
				del self.hero
		return self.visual_actions
		
	def createMobs(self):
		mobs = dict()
		for i in range(0,5):
			self.beings["enemy"+str(i)] = Monster(-0.10,0.5)
			self.beings["enemy"+str(i)].position["x"] = (random.random()*10) + 7
			self.beings["enemy"+str(i)].position["y"] = (random.random()*10) + 7
			mobs["enemy"+str(i)] = self.beings["enemy"+str(i)].position
		return mobs
		
class Entity():
	def __init__(self,speedXY,velocityZ):
		self.speedXY = speedXY
		self.velocityZ = velocityZ
		self.position = {"x":0,"y":0,"z":25}
		self.rotation = {"h":0,"p":0,"r":0}
		self.limitZ = 25.0
		self.jumping = False
		self.alive = True
		self.life = 100
		self.attacking = 0
		self.hurt = 0
		self.visual_actions = dict()
	
	#Analyse les actions (ou messages) recus de la vue pour déterminer les actions ou les contraintes du joueur et des ennemis
	def analyzeActions(self,actions):
		self.visual_actions = dict()
		self.limitZ = 0.0
		if "limitZ" in actions:
			self.limitZ = actions["limitZ"]
		if "fall" in actions:
			self.velocityZ = actions["fall"]
	
	def move(self):
		new_position = Misc.getAngledPoint(self.rotation["h"],self.speedXY,self.position["x"],self.position["y"])
		new_position["z"] = self.position["z"]
		self.position = new_position
		
	def jump(self):
		newZ = copy.copy(self.position["z"])
		if(self.jumping):
			newZ += self.velocityZ
			if(newZ < self.limitZ):
				newZ = self.limitZ
				self.jumping = False
				self.velocityZ = 0.0
			else:
				self.velocityZ = self.velocityZ - 0.025
		elif(newZ != self.limitZ):
			self.jumping = True
			self.velocityZ = -0.025
		
		if(newZ != self.position["z"]):
			self.position["z"] = newZ
			return True
		else:
			return False
		
class Monster(Entity):
	def __init__(self,speedXY,velocityZ):
		Entity.__init__(self,speedXY,velocityZ)
		self.target = None
		self.life = 30
		self.blocked = 0
		self.previous_position = self.position
	
	def analyzeActions(self,actions):
		Entity.analyzeActions(self,actions)
		self.previous_position = self.position
		if "blocked" in actions:
			self.blocked = 25
			self.rotation["h"] += actions["blocked"]
			self.position = self.previous_position
		if "hurt" in actions:
			if(self.hurt == 0):
				self.life -= 10
				self.position["x"] += actions["hurt"].x
				self.position["y"] += actions["hurt"].y
				self.hurt = 25
				self.visual_actions.update(hurt=True)
				if(self.life == 0):
					self.alive = False
					self.visual_actions.update(die=True)
		if "neartarget" in actions:
			self.target = actions["neartarget"]
		else:
			self.target = None
	
	#Selon l'analyse des actions, un déroulement s'ensuit pour déterminer la prochaine mise-à-jour à effectuer au monstre
	def loopActions(self):
		if(self.blocked > 0):
			self.blocked -=1
		elif(self.target != None):
			angle = Misc.calcAngle(self.position["x"],self.position["y"],self.target.x,self.target.y) + 90
			self.rotation["h"] = angle 
			if(Misc.calcDistance(self.position["x"],self.position["y"],self.target.x,self.target.y) < 2 and self.attack()):
				self.attacking = 60
				self.visual_actions.update(attack=True)
		elif(random.random() < 0.03):
			self.rotation["h"] = random.random()*360
		if(self.attacking == 0):
			self.move()
		else:
			self.attacking -=1
			if(self.attacking == 30):
				self.visual_actions.update(active=True)
			elif(self.attacking == 5):
				self.visual_actions.update(inactive=True)
		if(self.hurt > 0):
			if(self.hurt == 25):
				self.visual_actions.update(pushback=self.position)
			self.hurt-=1
			self.position = self.previous_position
			if(self.hurt < 10):
				self.visual_actions.update(hurt=False)
		else:
			self.visual_actions.update(move=self.position)
			self.visual_actions.update(rotate=self.rotation)
			
		return self.visual_actions
		
	def attack(self):
		if(self.attacking == 0):
			self.attacking = 60;
			return True
		else:
			return False
	
		
class Hero(Entity):
	def __init__(self):
		Entity.__init__(self,-0.25,0.5)
		self.life = 50
		self.cam_rotation = 0
		self.rotation["h"] = 0
		self.directions = list()
		
	def analyzeActions(self,actions):
		Entity.analyzeActions(self,actions)
		if "blocked" in actions:
			self.position["x"] = actions["blocked"]["x"]
			self.position["y"] = actions["blocked"]["y"]
		if "hurt" in actions:
			if(self.hurt == 0):
				self.life -= 10
				self.position["x"] += actions["hurt"].x
				self.position["y"] += actions["hurt"].y
				self.hurt = 25
				self.visual_actions.update(hurt=True)
				if(self.life == 0):
					self.alive = False
					self.visual_actions.update(die=True)
		if "jump" in actions:
			if(not self.jumping):
				self.jumping = True
				self.velocityZ = 0.5
		if "movement" in actions:
			self.changeDirection(actions["movement"])
		if "rotation" in actions:
			self.cam_rotation = actions["rotation"] #la rotation est changée chaque fois que la camera tourne
			#On utilise aussi cette derniere pour déterminer la nouvelle position du personnage à chacun de ses déplacements
			if(self.cam_rotation > 360):
				self.cam_rotation -= 360
			elif(self.cam_rotation < 0):
				self.cam_rotation +=360
		if "attack" in actions:
			result = self.attack()
			self.visual_actions.update(attack=result)
		
	#Même principe que la méthode de la classe Monster, seulement le traitement est différent pour refléter les contrôles du joueur 
	def loopActions(self):
		if(self.hurt == 25):
			self.visual_actions.update(pushback=self.position)
		if(self.hurt == 0 and len(self.directions) > 0):
			self.rotate()
			self.move()
			self.visual_actions.update(rotate=self.rotation)
			self.visual_actions.update(move=self.position)
		if(self.jump()):
			self.visual_actions.update(jump=self.position["z"])
		if(self.attacking > 0):
			self.attacking -=1
			if(self.attacking == 55):
				self.visual_actions.update(active=True)
			elif(self.attacking == 25):
				self.visual_actions.update(inactive=True)
		if(self.hurt > 0):
			self.hurt-=1
			if(self.hurt == 10):
				self.visual_actions.update(hurt=False)
		return self.visual_actions
		
	def changeDirection(self,movements):
		for move in movements:
			if(move[0] == "+"):
				self.directions.append(move[1])
			elif(move[0] == "-"):
				self.directions.remove(move[1])
			
	def rotate(self):
		newH = 0
		#la variable newH est pour déterminer si oui ou non on applique
		#la rotation du personnage visuellement (donc qu'on a utiliser le clavier)
		if("l" in self.directions):
			newH +=90
		elif("r" in self.directions):
			newH -=90
		if("f" in self.directions):
			newH /= 2
		elif("b" in self.directions):
			newH = ((newH/2)*-1) + 180
		
		newH += self.cam_rotation
		if(newH > 360):
			newH -= 360
		elif(newH < 0):
			newH += 360
		self.rotation["h"] = newH
		return newH
		
	def attack(self):
		if(self.attacking == 0):
			self.attacking = 60;
			return True
		else:
			return False
