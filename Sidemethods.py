import math

class Misc(object):
	
    def getAngledPoint(angle,longueur,cx,cy):
        radian = (angle+90)*(math.pi/180)#Changement en radian car math.cos et math.sin les utilise
        x = (math.cos(radian)*longueur)+cx
        y = (math.sin(radian)*longueur)+cy
        positions = {"x" : x, "y" : y, "z" : 0 }
        return positions
    getAngledPoint = staticmethod(getAngledPoint)
    
    def calcAngle(x1,y1,x2,y2):
         dx = x2-x1
         dy = y2-y1
         angle = (math.atan2(dy,dx) ) *(180/math.pi)
         return angle
    calcAngle = staticmethod(calcAngle)
    
    def calcDistance(x1,y1,x2,y2):
         dx = abs(x2-x1)**2
         dy = abs(y2-y1)**2
         distance=math.sqrt(dx+dy)
         return distance
    calcDistance = staticmethod(calcDistance)